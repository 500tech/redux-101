import React from 'react';

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <h1>Welcome to Redux 101</h1>
      </div>
    );
  }
}

export default App;
